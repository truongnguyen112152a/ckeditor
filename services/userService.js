const userModel = require('../models/userModel')

function getUserAll(name) {
    return userModel.findOne({
        username: name
    })
}
function createUser(username, arr) {
    return userModel.create({
        username: username,
        image: arr
    })
}
function update(name, data) {
    return userModel.updateOne({
        username: name
    }, {
        $push: {image: data}
    })
}
function remove(name) {
    return userModel.deleteOne({
        username: name
    })
}
function insert(content) {
    return userModel.insertMany({content: Buffer.alloc(50, content)})
}
module.exports = {
    getUserAll,
    createUser,
    update,
    remove,
    insert
}