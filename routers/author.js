const express = require('express')
const router = express.Router()
const user = require('../services/userService')
const account = require('../services/account')
const multer = require('multer');
const path = require('path')
const fs = require('fs');
const lodash = require('lodash')

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'fakeImgae')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + "-" + file.originalname)
    }
})

var upload = multer({ storage: storage })

// upload single file to disk
router.post('/uploadfile/:name', upload.single('myFile'), (req, res, next) => {
    const file = req.file
    if (!file) {
        const error = new Error('Please upload a file')
        error.httpStatusCode = 400
        return next(error)
    }
    let obj = {
        username: req.body.myData,
        image: file.filename
    }
    user.createUser(req.params.name, [file.filename])
    .then((result) => {
        res.redirect("/")
    }).catch((err) => {
        res.json(err)
    });
    // res.sendFile(path.join(`${__dirname}/uploadfile/${req.file.filename}`))
    // res.redirect("/")
})

// upload multi file to disk
router.post('/upload-multi', upload.array("myFile", 120),(req, res, next) => {
    const files = req.files
    console.log(files);
    // if (!files) {
    //     const error = new Error('Please upload a file')
    //     error.httpStatusCode = 400
    //     return next(error)
    // }
    // user.createUser(req.body.myData, files.map((value,index) => {
    //     return value.filename
    // }))
    // .then((result) => {
    //     res.redirect("/")
    // }).catch((err) => {
    //     res.json(err)
    // });
    // // res.sendFile(path.join(`${__dirname}/uploadfile/${req.file.filename}`))
    // res.send("upload file thành công")
})


// upload to DB
// var img = fs.readFileSync(req.file.path);
    // var encode_image = img.toString('base64');
    // // Define a JSONobject for the image attributes for saving to database

    // var finalImg = {
    //     contentType: req.file.mimetype,
    //     image: new Buffer(encode_image, 'base64')
    // };
router.post('/uploadphoto1', upload.single('myImage'), (req, res) => {
    const file = req.file
    if (!file) {
        const error = new Error('Please upload a file')
        error.httpStatusCode = 400
        return next(error)
    }
    let obj = {
        username: req.body.myData,
        image: file.filename
    }
    user.createUser(obj)
    .then((result) => {
        res.redirect("/")
    }).catch((err) => {
        res.json(err)
    });
})

// lấy data về
router.get("/image/:name", async (req, res) => {
    try {
        let data = await user.getUserAll(req.params.name)
        res.json({
            err: false,
            value: data
        })
    } catch (error) {
        res.json({
            err: true,
            message: error
        })
    }
})
router.post('/more/:name', upload.single('myFile'), async (req, res, next) => {
    const file = req.file
    if (!file) {
        const error = new Error('Please upload a file')
        error.httpStatusCode = 400
        return next(error)
    }
    let obj = {
        username: req.body.myData,
        image: file.filename
    }
    let find = await user.update(req.params.name, file.filename)
    // .then((result) => {
    //     res.redirect("/")
    // }).catch((err) => {
    //     res.json(err)
    // });
    // res.sendFile(path.join(`${__dirname}/uploadfile/${req.file.filename}`))
    res.redirect("/")
})
router.delete("/:name", async (req, res) => {
    try {
        let find = await user.getUserAll(req.params.name)
        if(find.username) {
            let data = await user.remove(req.params.name)
            if(data.deletedCount) {
                for(let i = 0; i < find.image.length; i++){
                    fs.unlink(`./uploadfile/${find.image[i]}`)
                }  
                // console.log(x);
                // if(x) return res.send("ok")
            }
        }
    } catch (error) {
        res.json({
            err: true,
            message: error
        })
    }
})
router.post("/account", (req, res) => {

})
module.exports = router