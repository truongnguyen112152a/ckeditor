const express = require('express')
const router = express.Router()

const author = require('./author')

router.use('/author', author)

module.exports = router

