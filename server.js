require('dotenv').config()
const express = require('express')
const app = express()
const router = require('./routers/index')
const bodyParser = require('body-parser')
const cors = require('cors')
const path = require('path')

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
// app.use(express.static(path.join(__dirname + "/uploadfile")))
app.use(express.static("uploadfile"))
app.use('/', router)
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/views/multer.html');
})

app.listen(process.env.PORT, () => console.log(`server connect port ${process.env.PORT}`))